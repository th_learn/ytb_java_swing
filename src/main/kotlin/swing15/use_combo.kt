package swing15

import base.SimpleFrame
import java.awt.BorderLayout
import javax.swing.DefaultComboBoxModel
import javax.swing.JComboBox

fun main() {
    SimpleFrame().apply {
        add(JComboBox<String>().apply {
            model = DefaultComboBoxModel<String>(arrayOf("a", "b", "c"))
        }, BorderLayout.EAST)
        isVisible = true
    }
}
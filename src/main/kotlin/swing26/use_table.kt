package swing26

import base.simpleAddAtEast
import java.awt.BorderLayout
import javax.swing.JScrollPane
import javax.swing.JTable
import javax.swing.table.AbstractTableModel


fun main() {
    simpleAddAtEast {
        it.add(JScrollPane(JTable().apply {
            model = object: AbstractTableModel() {
                val data = listOf<List<String>>(
                    listOf("zhangsan", "33"),
                    listOf("zhaoyun", "28")
                )
                override fun getRowCount(): Int {
                    return data.size
                }

                override fun getColumnCount(): Int {
                    return data[0].size
                }

                override fun getValueAt(rowIndex: Int, columnIndex: Int): Any {
                    return data[rowIndex][columnIndex]
                }

                override fun getColumnName(column: Int): String {
                    return when(column) {
                        0 -> "name"
                        1 -> "age"
                        else -> "unknown"
                    }
                }

            }.apply {
                fireTableDataChanged()
            }
        }), BorderLayout.EAST)
    }
}
package swing19

import base.SimpleFrame
import base.simpleAddAtEast
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.KeyEvent
import java.security.KeyStore
import javax.swing.*

fun main() {
    SimpleFrame("mnemonic", false).apply {
        this.jMenuBar = JMenuBar().apply {
            this.add(JMenu("File").apply {
                this.mnemonic = KeyEvent.VK_F
                this.add(JMenu("New").apply {
                    this.add(JMenuItem("File"))
                    this.add(JMenuItem("Project"))
                })
                this.add(JSeparator())
                this.add(JMenuItem("Exit").apply {
                    this.mnemonic = KeyEvent.VK_X
                    this.accelerator = KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.CTRL_MASK)
                    this.addActionListener {
                        println("exit")
                    }
                })
            })
        }

        this.add(JPanel().apply {
            add(JPanel().apply {
                layout = FlowLayout()
                add(JLabel("username: ").apply {
                    displayedMnemonic = KeyEvent.VK_U
                })
                add(JTextField(30))
            })

            add(JPanel().apply {
                val field = JTextField(30)
                layout = FlowLayout()
                add(JLabel("password: ").apply {
                    displayedMnemonic = KeyEvent.VK_P
                    labelFor = field
                })
                add(field)
            })

        }, BorderLayout.CENTER)

        isVisible = true
    }
}
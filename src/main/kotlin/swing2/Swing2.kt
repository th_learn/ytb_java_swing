package swing2

import base.SimpleFrame
import java.awt.BorderLayout
import java.awt.Label
import java.awt.TextArea
import javax.swing.BoxLayout

fun main() {
    val frame = SimpleFrame("swing2")
    val s_label = Label("south")
    val n_button = Label("click me")
    val t_middle = TextArea()
    var e_label = Label("east")
    var w_label = Label("west")
    frame.add(s_label, BorderLayout.NORTH)
    frame.add(n_button, BorderLayout.SOUTH)
    frame.add(t_middle, BorderLayout.CENTER)
    frame.add(e_label, BorderLayout.EAST)
    frame.add(w_label, BorderLayout.WEST)
    frame.isVisible = true
}
package swing20

import base.simpleAddAtEast
import java.awt.CheckboxMenuItem
import java.awt.Menu
import java.awt.MenuBar
import java.awt.MenuItem
import java.awt.event.KeyEvent
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem
import javax.swing.JSeparator

fun main() {
    simpleAddAtEast {
        it.jMenuBar = JMenuBar().apply {
            this.add(JMenu("File").apply {
                this.mnemonic = KeyEvent.VK_F
                this.add(JMenu("New").apply {
                    this.add(JMenuItem("File"))
                    this.add(JMenuItem("Project"))
                })
                this.add(JSeparator())
                this.add(JMenuItem("Exit"))
            })
        }
    }
}
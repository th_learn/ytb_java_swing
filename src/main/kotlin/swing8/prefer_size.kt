package swing8

import base.SimpleFrame
import java.awt.BorderLayout
import java.awt.TextArea
import javax.swing.JPanel

open class LeftJPanel: JPanel() {
    init {
        preferredSize.apply {
            this.width = 200
            preferredSize = this
        }
    }
}

fun main() {
    val root = SimpleFrame("set prefer size")
    root.add(TextArea(), BorderLayout.CENTER)
    root.add(LeftJPanel(), BorderLayout.WEST)
    root.isVisible = true
}


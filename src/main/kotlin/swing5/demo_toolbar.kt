package swing5

import base.SimpleFrame
import java.awt.BorderLayout
import java.awt.Button
import java.awt.FlowLayout
import javax.swing.JPanel

class MyToolBar: JPanel() {
    init {
        this.layout = FlowLayout(FlowLayout.LEFT)
        add(Button("File"))
        add(Button("Edit"))
    }

}

fun main() {
    var root = SimpleFrame("use toolbar")
    root.add(MyToolBar(), BorderLayout.NORTH)
    root.isVisible = true
}
package swing21

import base.simpleAddAtEast
import java.awt.Dimension
import javax.swing.JButton
import javax.swing.JOptionPane

fun main() {
    simpleAddAtEast {
        it.add(JButton("Ask me").apply {
            this.addActionListener {
                JOptionPane.showConfirmDialog(this, "You real want ask me?", "Info", JOptionPane.YES_NO_OPTION)
                JOptionPane.showInputDialog("What's your question", JOptionPane.INFORMATION_MESSAGE)
            }
        })
        it.minimumSize = Dimension(200, 100)
    }
}
package swing16

import base.simpleAddAtEast
import java.awt.BorderLayout
import javax.swing.JCheckBox

fun main() {
    simpleAddAtEast {
        JCheckBox("use quick?").apply {
            addActionListener {
                println("isChecked: ${this.isSelected}")
            }
            it.add(this, BorderLayout.EAST)
        }
    }
}

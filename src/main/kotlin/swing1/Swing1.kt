package swing1

import javax.swing.JFrame
import javax.swing.SwingUtilities

fun main() {
    SwingUtilities.invokeLater {
        val root = JFrame("Hello World")
        root.setSize(800, 600)
        root.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        root.isVisible = true
    }
}
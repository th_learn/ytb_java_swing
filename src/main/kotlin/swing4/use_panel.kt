package swing4

import base.SimpleFrame
import java.awt.BorderLayout
import javax.swing.JButton
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JTextArea

class UsePanelSimplePanel: JPanel() {
    init {
        this.layout = BorderLayout()
        this.add(JScrollPane(JTextArea()))
    }
}

fun main() {
    val simpleFrame = SimpleFrame("use panel")
    simpleFrame.layout = BorderLayout()
    simpleFrame.add(UsePanelSimplePanel(), BorderLayout.CENTER)

    simpleFrame.add(JButton("click me"), BorderLayout.SOUTH)
    simpleFrame.isVisible = true
}
package swing17

import base.simpleAddAtEast
import javafx.scene.control.RadioButton
import org.intellij.lang.annotations.Flow
import java.awt.BorderLayout
import java.awt.FlowLayout
import javax.swing.BoxLayout
import javax.swing.ButtonGroup
import javax.swing.JPanel
import javax.swing.JRadioButton

fun main() {
    simpleAddAtEast(JPanel().apply {
        val bg = ButtonGroup()
        val container = JPanel().apply {
            layout = BoxLayout(this, BoxLayout.Y_AXIS)
            add(JRadioButton("option 1").apply {
                actionCommand = "the option 1"
                bg.add(this)
            })

            add(JRadioButton("option 2").apply {
                actionCommand = "the option 2"
                bg.add(this)
            })
        }
        this.add(container, BorderLayout.EAST)
    })
}
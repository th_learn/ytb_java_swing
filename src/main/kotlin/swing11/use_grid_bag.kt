package swing11

import base.SimpleFrame
import swing9.BorderPanel
import java.awt.*

open class UserInfoGridBagPanel: BorderPanel() {
    init {
        layout = GridBagLayout()

        addLine("username", "unknown", 0)

        add(Label(), GridBagConstraints().apply {
            this.gridx = 1
            this.gridy = 2
            this.weighty = 100.0
        })
    }

    private fun addLine(left: String, right: String, lineNum: Int) {
        add(Label(left), GridBagConstraints().apply {
            this.gridx = 0
            this.gridy = lineNum
            this.insets = Insets(0, 0, 0, 5)
        })

        add(Label(right), GridBagConstraints().apply {
            this.gridx = 1
            this.gridy = lineNum
            this.insets = Insets(0, 5, 0, 0)
        })
    }
}


fun main() {
    val root = SimpleFrame("set prefer size")
    root.add(UserInfoGridBagPanel(), BorderLayout.WEST)
    root.isVisible = true
}
package swing18

import base.simpleAddAtEast
import java.awt.Menu
import java.awt.MenuBar
import java.awt.MenuItem
import javax.swing.JMenu
import javax.swing.JMenuBar
import javax.swing.JMenuItem

fun main() {
    simpleAddAtEast {
        it.jMenuBar = JMenuBar().apply {
            this.add(JMenu("File").apply {
                this.add(JMenu("New").apply {
                    this.add(JMenuItem("File"))
                    this.add(JMenuItem("Project"))
                })
            })
        }

        it.menuBar = MenuBar().apply {
            this.add(Menu("File").apply {
                this.add(Menu("New").apply {
                    this.add(MenuItem("File"))
                    this.add(MenuItem("Project"))
                })
            })
        }
    }
}
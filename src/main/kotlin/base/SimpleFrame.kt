package base

import java.awt.BorderLayout
import java.awt.TextArea
import javax.swing.JComponent
import javax.swing.JFrame
import javax.swing.JPanel

class SimpleFrame(title: String = "Demo", addCenter: Boolean = true) : JFrame(title) {

    init {
        this.setSize(800, 600)
        this.defaultCloseOperation = EXIT_ON_CLOSE
        if (addCenter) {
            this.add(TextArea(), BorderLayout.CENTER)
        }
    }

}

fun simpleAddAtEast(runnable: (root: JFrame) -> Unit) {
    SimpleFrame().apply {
        runnable.invoke(this)
        isVisible = true
    }
}

fun simpleAddAtEast(panel: JComponent) {
    SimpleFrame().apply {
        add(panel, BorderLayout.EAST)
        isVisible = true
    }
}

package swing22

import base.simpleAddAtEast
import javax.swing.JButton
import javax.swing.JFileChooser
import javax.swing.JPanel

fun main() {
    simpleAddAtEast(JPanel().apply {
        add(JButton("open file").apply {
            addActionListener {
                JFileChooser().apply {
                    showOpenDialog(this)
                    println(selectedFile)
                }
            }
        })

        add(JButton("save file").apply {
            addActionListener {
                JFileChooser().apply {
                    showSaveDialog(this)
                    println(selectedFile)
                }
            }
        })
    })
}
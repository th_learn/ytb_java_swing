package swing13

import base.SimpleFrame
import swing11.UserInfoGridBagPanel
import java.awt.BorderLayout
import java.awt.GridBagConstraints
import javax.swing.JList

class LogPanel: JList<String>() {
    init {
        setListData(arrayOf("Player is walking", "Please has reach", "click npc"))
    }
}

class LeftPanelWithLog: UserInfoGridBagPanel() {
    init {
        add(LogPanel(), GridBagConstraints().apply {
            this.gridx = 0
            this.gridy = 1
        })
    }
}

fun main() {
    val root = SimpleFrame("set prefer size")
    root.add(LeftPanelWithLog(), BorderLayout.WEST)
    root.isVisible = true
}

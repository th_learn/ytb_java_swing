package swing28

import base.simpleAddAtEast
import java.awt.BorderLayout
import javax.swing.JButton
import javax.swing.JLabel
import javax.swing.JPopupMenu

fun main() {
    simpleAddAtEast {
        it.add(JButton("popup").apply {
            addActionListener {
                JPopupMenu().apply {
                    add(JLabel("hi, I'm pop out"))
                }.show(this, this.x + 100, this.y)
            }
        }, BorderLayout.EAST)
    }
}
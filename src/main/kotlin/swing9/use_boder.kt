package swing9

import base.SimpleFrame
import swing8.LeftJPanel
import java.awt.BorderLayout
import java.awt.TextArea
import javax.swing.BorderFactory

open class BorderPanel: LeftJPanel() {
    init {
        border = BorderFactory.createCompoundBorder(
            BorderFactory.createEmptyBorder(0, 5, 5, 5),
            BorderFactory.createTitledBorder("User info")
        )
    }
}

fun main() {
    val root = SimpleFrame("set prefer size")
    root.add(BorderPanel(), BorderLayout.WEST)
    root.isVisible = true
}

